package br.ucsal.bes20202.testequalidade.locadora;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20202.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20202.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20202.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.bes20202.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20202.testequalidade.locadora.persistence.VeiculoDAO;

/**
 * Testes INTEGRADOS para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
public class LocacaoBOIntegradoTest {

	private static LocacaoBO locacaoBO;
	private static VeiculoDAO veiculoDAO;
	
	private static List<String> placas = new ArrayList<String>();
	private LocalDate data = LocalDate.now();

	@BeforeAll
	public static void setupAll() {

		veiculoDAO = new VeiculoDAO();
		locacaoBO = new LocacaoBO(veiculoDAO);

		Veiculo veiculo1 = VeiculoBuilder.umVeiculo().fabricadoEm(2019).comPlaca("BFA1J022")
				.disponivel().build();
		Veiculo veiculo2 = VeiculoBuilder.umVeiculo().fabricadoEm(2019).comPlaca("KFN7A332")
				.disponivel().build();
		Veiculo veiculo3 = VeiculoBuilder.umVeiculo().fabricadoEm(2012).comPlaca("PKA5J802")
				.disponivel().build();
		Veiculo veiculo4 = VeiculoBuilder.umVeiculo().fabricadoEm(2012).comPlaca("HFG1G393")
				.disponivel().build();
		Veiculo veiculo5 = VeiculoBuilder.umVeiculo().fabricadoEm(2012).comPlaca("HJN4H047")
				.disponivel().build();

		veiculoDAO.insert(veiculo1);
		veiculoDAO.insert(veiculo2);
		veiculoDAO.insert(veiculo3);
		veiculoDAO.insert(veiculo4);
		veiculoDAO.insert(veiculo5);

		placas.add(veiculo1.getPlaca());
		placas.add(veiculo2.getPlaca());
		placas.add(veiculo3.getPlaca());
		placas.add(veiculo4.getPlaca());
		placas.add(veiculo5.getPlaca());

	}

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 * 
	 * @throws VeiculoNaoEncontradoException
	 */
	@Test
	@DisplayName("Teste integrado do valor total: período de 3 dias de locação para 5 veículos")
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() throws VeiculoNaoEncontradoException {
		Double valorEsperado = 1410.00;

		Double valorAtual = locacaoBO.calcularValorTotalLocacao(placas, 3, data);
		Assertions.assertEquals(valorEsperado, valorAtual);

	}

}
